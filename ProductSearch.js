import React from 'react';
import {Card, Input} from "antd";

const Search = Input.Search;

class ProductSearch extends React.Component{
	state = { url: '' };

	onInputChange = event => {
		this.setState({ url: event.target.value })
	};

	onFormSubmit = (event) => {
		event.preventDefault();
		this.props.onSubmit(this.state.url)
	}

	render(){
		return (
				<Card className="gx-card" title="Search Product">
				<form onSubmit={this.onFormSubmit}>
			      <Search
			        placeholder="Product Link: ex. http://www.aliexpres.com/item/name/id.html"
			        value={this.state.url} 
					onChange={this.onInputChange}
			      />
			      </form>
			    </Card>
			);
		}
}

export default ProductSearch;