import React, {Component} from "react";
import axios from 'util/Api';
import ProductSearch from "./ProductSearch";


class SamplePage extends Component {
	onSearchSubmit(url){
		axios.get('/scrap', {
      	params: {
        	url: url
      	}
    });
	}

	render(){
		return (
			<div className="ui container" style={{ marginTop: '10px'}}>
				<ProductSearch onSubmit={this.onSearchSubmit} />
			</div>
			);
	}
} 

export default SamplePage;
